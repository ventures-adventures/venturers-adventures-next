import React from 'react'

export interface ITextStyleProps extends React.CSSProperties {
  align?: "left" | "right" | "center"
  bold?: boolean
  italic?: boolean
  underline?: boolean
  inline?: boolean
}

export const TextStyle: React.FC<ITextStyleProps> = ({
  align = "left" as "left",
  bold = false,
  color,
  inline = false,
  italic = false,
  fontSize = 1.2,
  lineHeight = 1.2,
  underline,
  children
}) => {


  const Tag = inline
    ? 'span'
    : 'div'

  return (
    <>
      <Tag className="text">{children}</Tag>
      <style jsx>{`
        .text {
          color: ${color ? color : 'inherit'};
          font-weight: ${bold ? 'bold' : 'inherit'};
          font-style: ${italic ? 'italic' : 'normal'}
          font-size: ${fontSize}rem;
          line-height: ${lineHeight};
          text-align: ${align ? align : 'left'};
          text-decoration: ${underline ? 'underline' : 'inherit'}
        }
      `}</style>
    </>
  )
}
