import React from 'react'

export const Row : React.FC = ({children, ...props}) => (
    <>
        <section className='row'>
            {children}
        </section>
        <style jsx>{`
            section {
                display: flex;
                flex-direction: row;
                width: 100%;
                max-width: 1100px;
            }
            section > * {
                flex-grow: 1;
            }
        `}</style>
    </>
)
