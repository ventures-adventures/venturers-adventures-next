import Link from 'next/link'

export interface InlineLinkProps {
  href: string;
  title?: string;
}

export const InlineLink: React.FC<InlineLinkProps> = ({ href, title, children }) => (
  <Link href={href.replace('https://ventures-adventures.com', '')}><a title={title}>{children}</a></Link>
)
