import React, { ReactNode } from 'react'

export interface IHeadingProps {
  content: ReactNode;
  h?: 1|2|3|4|5|6;
  color?: string;
  size?: 'xs'|'s'|'m'|'l'|'xl';
  align?: 'l'|'c'|'r';
}

export const Heading: React.FC<IHeadingProps> = ({ content, h = 1, color, size = 'm', align = 'c' }) => {
  return (
    <>
      {
        h === 1
          ? <h1 className={`title size-${size}`}>{content}</h1>
          : h === 2
            ? <h2 className={`title size-${size}`}>{content}</h2>
            : h === 3
              ? <h3 className={`title size-${size}`}>{content}</h3>
              : h === 4
                ? <h4 className={`title size-${size}`}>{content}</h4>
                : h === 5
                  ? <h5 className={`title size-${size}`}>{content}</h5>
                  : h === 6
                    ? <h6 className={`title size-${size}`}>{content}</h6>
                    : <h1 className={`title size-${size}`}>{content}</h1>
      }
      <style jsx>{`

        .title a {
          color: ${color ? color : '#222222'};
          text-decoration: none;
        }

        .title a:hover,
        .title a:focus,
        .title a:active {
          text-decoration: underline;
        }

        .title {
          color: ${color ? color : 'inherit'};
          margin: 0;
          line-height: 1.5;
          text-align: ${
            align === 'l'
              ? 'left'
              : align === 'c'
                ? 'center'
                : align === 'r'
                  ? 'right'
                  : 'center'
          };
        }

        .size-xs {
          font-size: 1.2rem;
          margin-top: 1.2rem;
          margin-bottom: 0.6rem;
          text-shadow: 0px 0px 0.6rem rgba(255, 255, 255, 1);
        }
        .size-s {
          font-size: 1.8rem;
          margin-top: 1.8rem;
          margin-bottom: .9rem;
          text-shadow: 0px 0px 0.9rem rgba(255, 255, 255, 1);
        }
        .size-m {
          font-size: 2.6rem;
          margin-top: 2.6rem;
          margin-bottom: 1.2rem;
          text-shadow:
            0px 0px 0.6rem rgba(255, 255, 255, 1),
            0px 0px 1.2rem rgba(255, 255, 255, 1),
            0px 0px 2.6rem rgba(255, 255, 255, 1);
        }
        .size-l {
          font-size: 4rem;
          margin-top: 4rem;
          margin-bottom: 2rem;
          text-shadow: 0px 0px 2rem rgba(255, 255, 255, 1);
        }
        .size-xl {
          font-size: 6rem;
          margin-top: 6rem;
          margin-bottom: 3rem;
          text-shadow: 0px 0px 3rem rgba(255, 255, 255, 1);
        }

        @media (max-width: 600px) {
      `}</style>
    </>
  )
}
