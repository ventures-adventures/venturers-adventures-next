import React from 'react'
import Link from 'next/link'
import { Heading, IHeadingProps } from './'

export interface ITitleProps extends IHeadingProps {
  href?: string;
}

export const Title: React.FC<ITitleProps> = ({ content, h = 1, color, size = 'm', align = 'c', href }) => {

  return (
    <>
    {href
      ? <Heading h={h} content={<Link href={href}><a>{content}</a></Link>} color={color} size={size} align={align} />
      : <Heading h={h} content={content} color={color} size={size} align={align} />
    }
    <style jsx>{`
      a {
        text-decoration: none;
        color: ${color ? color : 'inherit'};
        background-image: linear-gradient(120deg,#8AFF80,#8AFF80);
        background-repeat: no-repeat;
        background-size: 100% .4em;
        background-position: 0 88%;
        transition: background-size 0.2s ease;
      }

      a:hover {
        background-image: linear-gradient(270deg, #FFFF80, #8aff80);
        background-size: 600% 600%;
        transition: background-size 1s ease;

        -webkit-animation: HoverGradient 10s ease infinite;
        -moz-animation: HoverGradient 10s ease infinite;
        animation: HoverGradient 10s ease infinite;
      }

      @-webkit-keyframes HoverGradient {
          0%{background-position:0% 50%}
          50%{background-position:100% 50%}
          100%{background-position:0% 50%}
      }
      @-moz-keyframes HoverGradient {
          0%{background-position:0% 50%}
          50%{background-position:100% 50%}
          100%{background-position:0% 50%}
      }
      @keyframes HoverGradient {
          0%{background-position:0% 50%}
          50%{background-position:100% 50%}
          100%{background-position:0% 50%}
      }
    `}</style>
    </>
  )

}
