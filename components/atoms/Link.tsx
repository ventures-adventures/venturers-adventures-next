import NextLink from 'next/link'
import packageJson from '../../package.json';

export interface ILinkProps {
  href: string;
  title?: string;
}

export const Link: React.FC<ILinkProps> = ({ href, title, children }) => (
  <NextLink href={href.replace('https://ventures-adventures.com', '')}><a title={title}>{children}</a></NextLink>
)