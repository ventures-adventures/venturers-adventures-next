import React from 'react'

export const Column : React.FC = ({children, ...props}) => (
    <>
        <section className="column">
            {children}
        </section>
        <style jsx>{`
            section.column {
                display: flex;
                flex-direction: column;
                flex-grow: 1;
            }
        `}</style>
    </>
)