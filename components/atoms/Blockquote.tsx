import React from 'react'

export const Blockquote: React.FC = ({ children }) => (
  <div className="blockquote-wrapper">
    <blockquote>
      {children}
    </blockquote>
    <style jsx>{`
      .blockquote-wrapper {
        border-left: 0.5rem solid #9580FF;
      }
      blockquote {
        font-style: italic;
        line-height: 1.8;
      }
      :global(.blockquote-wrapper > blockquote > *) {
        font-size: 1.3em;
      }
      :global(.blockquote-wrapper > blockquote em) {
        font-weight: bold;
      }
    `}</style>
  </div>
)
