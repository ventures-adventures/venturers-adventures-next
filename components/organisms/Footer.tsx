import { Column, Link, InlineLink, Row, TextStyle  } from "../atoms"

export const Footer = () => (
  <>  
    <footer>
        <Row>
          <Column>
            <p>Collections:</p>
            <Column>
              <ul>
                <li key="design-systems"><Link href="/design-systems">Design systems</Link></li>
                <li key="tech"><Link href="/tech">Technologies</Link></li>
                <li key="bookmarks"><Link href="/bookmarks">Bookmarks</Link></li>
              </ul>
            </Column>            
          </Column>
          <Column>
            <p>Contact:</p>
            <div className=''>
              <a href="https://twitter.com/jordanskole">Twitter</a>
            </div>
          </Column>
          <Column></Column>
        </Row>
        <p>This site doesn't use trackers of any kind. If you see something you like <a href="https://twitter.com/intent/tweet?text=Hey%20@jordanskole%20I%20like%20the%20site">send me a tweet</a>.</p>              
    </footer>
    <style jsx>{`
      footer {
        width: 100%;
        border-top: 1px solid #eaeaea;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
      }

      footer img {
        margin-left: 0.5rem;
      }

      footer ul {
        list-style-type: none;
        padding-left: 0px;
        margin-top: 0px;
      }

    `}</style>
  </>
)
