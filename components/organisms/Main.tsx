export const Main: React.FC = ({ children }) => (
  <>
    <main>
      {children}
    </main>
    <style jsx>{`
      main {
        padding: 5rem 0;
        flex: 1;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
      }
    `}</style>
  </>
)
