import { Card, ICard } from '../'

export interface IGridProps {
  cards: ICard[];
  columns?: 1|2;
}

export const CardGrid: React.FC<IGridProps> = ({ cards, columns }) => (
  <>
    <div className="grid">
      {cards.map(({ title, href, imgSrc, imgSrcSet, imgAlign, content }, i) => (
        <Card title={title} href={href} imgSrc={imgSrc} imgSrcSet={imgSrcSet} imgAlign={imgAlign} key={i}>
          <div dangerouslySetInnerHTML={{ __html: content }} />
        </Card>
      ))}
    </div>
    <style jsx>{`
      .grid {
        display: flex;
        align-items: center;
        justify-content: center;
        flex-wrap: wrap;
        max-width: 1100px;
      }
      :global(.grid > .card) {
        flex-basis: ${columns === 2 ? '45%' : '95%'};
      }
      @media (max-width: 600px) {
        .grid {
          width: 100%;
          flex-direction: column;
        }
      }
    `}</style>
  </>
)
