import Link from 'next/link'
import { Title } from '../'

interface IArticleTitleProps {
  title: string;
  author: Author;
  publishDate: string;
}

type Author = {
  firstName: string;
  lastName: string;
  slug: string;
}

export const ArticleTitle: React.FC<IArticleTitleProps> = ({ title, author, publishDate }) => (
  <div className="title-wrapper">
    <Title h={1} content={title} />
    <Title h={3}
      content={<>Published by <Link href={`/authors/${author.slug}`}><a>{author.firstName} {author.lastName}</a></Link> on {new Date(publishDate).toLocaleDateString()}</>}
      size='xs'
      color='#424242' />
      <style jsx>{`
        .title-wrapper {
          max-width: 1100px;
          margin: 0 auto;
        }
      `}</style>
  </div>
)
