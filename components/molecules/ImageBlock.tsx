interface IImageBlockProps {
  content?: string;
  caption?: string;
  size?: string;
  src: string;
  srcSet?: string;
}

export const ImageBlock = ({ content, caption, size, src, srcSet }) => (
  <section>
    <div className="image-wrapper">
      <img src={src} srcSet={srcSet} />
      <p>{caption}</p>
    </div>
    <style jsx>{`
      section {

      }
      img {
        max-width: 100%;
      }
      .image-wrapper {
        max-width: ${ size === 'heading' ? '1100px' : size === 'body' ? '600px' : '100%'};
        margin: 0 auto;
      }
      .image-wrapper p {
        font-size: 0.8rem;
        color: #222222;
        text-align: center;
        font-style: italic;
      }
    `}</style>
  </section>
)
