import React, { forwardRef } from 'react'
import Link from 'next/link'
import { Heading } from '../'

interface IRootCardProps {
  href?: string;
  color?: string;
  imgSrc?: string;
  imgSrcSet?: string;
  imgAlign?: 'left'|'right'|'center';
}

export interface ICard extends IRootCardProps {
  title?: string;
  content?: string;
}

export const Card: React.FC<ICard> = ({ children, title, href, imgSrc, imgSrcSet, imgAlign, color }) => (
  <RootCard href={href} imgSrc={imgSrc} imgSrcSet={imgSrcSet} >
    {title
      ? <Heading h={3} content={title} size='xs' align='l' />
      : null
    }
    <div className="content-wrapper">
      {children}
    </div>
    <style jsx>{`
      .content-wrapper {
        padding-bottom: 1.2rem;
      }
    `}</style>
  </RootCard>
)


interface IInnerCard {
  color?: string;
  href?: string;
  imgSrc?: string;
  imgSrcSet?: string;
  imgAlign?: 'left' | 'center' | 'right';
}
const InnerCard: React.FC<IInnerCard> = ({children, color, href, imgSrc, imgSrcSet, imgAlign = 'left' }) => (
  <>
    <a className="card" href={href}>
      <div className='inner-card-border'>
        <div className="inner-card">
          {imgSrc ? (<img src={imgSrc} />): null}
          <div className="card-content">
            {children}
          </div>
        </div>
      </div>
    </a>
    <style jsx>{`

    .card {
      background: none;
      margin: 1rem;
      padding: .3rem;
      box-sizing: border-box;
      transition: background-color 0.15s ease;
      text-decoration: none;
      color: inherit;
    }

    .card:hover,
    .card:focus,
    .card:active {
      /* color: ${color ? color : '#454158'}; */
      background: linear-gradient(270deg, #8aff80, #9580ff, #ff5555);
      background-size: 600% 600%;

      -webkit-animation: HoverGradient 10s ease infinite;
      -moz-animation: HoverGradient 10s ease infinite;
      animation: HoverGradient 10s ease infinite;
    }
    @-webkit-keyframes HoverGradient {
        0%{background-position:0% 50%}
        50%{background-position:100% 50%}
        100%{background-position:0% 50%}
    }
    @-moz-keyframes HoverGradient {
        0%{background-position:0% 50%}
        50%{background-position:100% 50%}
        100%{background-position:0% 50%}
    }
    @keyframes HoverGradient {
        0%{background-position:0% 50%}
        50%{background-position:100% 50%}
        100%{background-position:0% 50%}
    }

    .inner-card-border {
      padding: 1px;
      background: #eaeaea;
    }

    .inner-card-border:hover, 
    .inner-card-border:focus, 
    .inner-card-border:active {
      background: none;
    } 

    .inner-card {
      background-color: rgba(255, 255, 255, 1);
      display: flex;
      flex-direction: row;
      flex-basis: auto;      
      text-align: left;
      color: inherit;
      text-decoration: none;      
      transition: background-color 0.15s ease, color 0.15s ease, border-color 0.15s ease, border-width 0.15s ease;
    }

    .inner-card:hover,
    .inner-card:focus,
    .inner-card:active {
      background-color: rgba(255, 255, 255, 0.9);      
      /* border-color: ${color ? color : '#0070f3'}; */
    }

    .inner-card > img {
      flex-grow: 1;
      max-width: 33%;
      display: block;
      object-fit: cover;
    }

    .card-content {
      flex-grow: 2;
      width: 100%;
      padding: 1.5rem;
    }

    .card-content:hover,
    .card-content:focus,
    .card-content:active  {
      
    }

    .card p {
      margin: 0;
      font-size: 1.25rem;
      line-height: 1.5;
    }    

  `}</style>
  </>
) 

const RootCard: React.FC<IRootCardProps> = ({ href, color, imgSrc, imgSrcSet, children }) => (<InnerCard children={children} href={href} imgSrc={imgSrc} imgSrcSet={imgSrcSet} />)