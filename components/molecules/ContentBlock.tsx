import Link from 'next/link'
import ReactMarkdown from 'react-markdown'
import { Blockquote, InlineLink, Title } from '../'

export interface IContentBlockProps {
  id?: string;
  headline?: string;
  content?: string;
  citations?: Citation[];
}

type Citation = {

}

export const ContentBlock = ({ _id, headline, content, citations }) => (
  <section key={_id} id={_id}>
    {headline
      ? <Title content={headline} h={2} size='s' />
      : null
    }
    <div className="content-body">
      {content ? <ReactMarkdown source={content} escapeHtml={false} renderers={{ link: InlineLink, blockquote: Blockquote }} /> : null}
      {citations
        ? (
          <div className="citations">
            <ol>
              {citations.map(({ description, source, id }) => (
                <li key={id} ><Link href={source}><a>{description}</a></Link></li>
              ))}
            </ol>
          </div>
        ) : null
      }
    </div>
    <style jsx>{`
      section {
        max-width: 1100px;
        margin: 0 auto;
      }
      .content {
        margin-top: 6rem;
      }
      .content-body {
        max-width: 600px;
        margin: 0 auto;
      }
      .citations li {
        font-size: 0.8rem;
        color: #222222;
      }
    `}</style>
  </section>
)
