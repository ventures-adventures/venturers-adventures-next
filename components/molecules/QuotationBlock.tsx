import ReactMarkdown from 'react-markdown'
import { Title } from '../'

export const QuotationBlock = ({ quote, author }) => (
  <section>
    <div className="quote-wrapper">
      <ReactMarkdown source={quote} />
      <Title content={<>~{author}</>} size='xs' />
    </div>
    <style jsx>{`
      section {
        background: #80FFEA;
        padding: 3rem 0;
      }
      .quote-wrapper {
        max-width: 1100px;
        margin: 0 auto;
      }
      p {
        font-size: 1.4em;
        font-style: italic; font-weight: bold; text-align: center; }
    `}</style>
  </section>
)
