import particles from 'particles.js'
import Head from 'next/head'
import getMetaData from 'metadata-scraper'
import { Card, CardGrid, ICard, Layout, Main, Title, Footer } from '../components'
import { request } from "../lib/datocms";

interface ITechProps {
    tech: ITech[]
    data?: any;
}

interface ITech {
    title: string;
    href: string;
    imgSrc?: string;
}

// const DynamicComponent = dynamic(() => import('particles.js/particles.js'))

const Technologies: React.FC<ITechProps> = ({ tech, data }) => {
  
  return (
    <Layout>
      <div>
        <Title href="/" content="Ventures & Adventures" size='xs' />
      </div>
      <Main>
        <Title content="Technologies" />
        <p className="description">
          A collection of very cool "products," however these items don't need to be for sale. For example it could be an interesting open-source software library, or even a simple CAD file. 
        </p>
        <CardGrid cards={tech} />
      </Main>
      <Footer />
      <style jsx>{`
        p.description {
          max-width: 1100px;
        }

        a {
          color: inherit;
          text-decoration: none;
        }

        a:not([href*='ventures-adventures.com']):not([href^='#']):not([href^='/']) .card h3:after {
          font-family: 'FontAwesome';
          content: " \f08e";
        }

      `}</style>

    </Layout>
  )
}

const INDEX_QUERY = `
query {
    allTechnologies {    
        title
        externalUrl
        slug
    }
    
    _allTechnologiesMeta {
        count
    }
}
`

export const getStaticProps = async () => {
  // Get external data from the file system, API, DB, etc.
  const data : any = await request({
    query: INDEX_QUERY,
    variables: {},
    preview: false
  });
  // The value of the `props` key will be

  const tech = await Promise.all(data.allTechnologies.map(async tech => {
    const metadata = await getMetaData(tech.externalUrl)

    return { title: tech.title, href: tech.externalUrl, imgSrc: metadata.image ? metadata.image : null, content: metadata.description ? metadata.description : null }
  }))
  
  //  passed to the `technologies` component  
  return {
    props: {
      data,
      tech
    }
  }
}

export default Technologies
