import Link from 'next/link'
import useRouter from 'next/router'

import ReactMarkdown from 'react-markdown'
import { request } from "../../lib/datocms";
import { ArticleTitle, ContentBlock, ImageBlock, Layout, Main, Title, QuotationBlock , Footer} from '../../components'


interface IArticleProps {
  data?: any;
}

const Article: React.FC<IArticleProps> = ({ data }) => {
  const { article } = data
  return (
  <Layout>
    <div>
      <Title href="/" content="Ventures & Adventures" size='xs' />
    </div>
    <Main>
      <article>
        <ArticleTitle author={article.author} title={article.title} publishDate={article._createdAt} />
        <div className="content">
          {article.block.map(({__typename, _id, headline, content, author, citations, quote, media, caption, size }) => {

            if (__typename === 'ContentRecord') return <ContentBlock _id={_id} headline={headline} content={content} citations={citations} />
            if (__typename === 'QuotationRecord') return <QuotationBlock author={author} quote={quote} />
            if (__typename === 'ImageRecord') {
              const { responsiveImage: { src, srcSet }} = media;
              return <ImageBlock src={src} srcSet={srcSet} caption={caption} size={size} content={content} />
            }
          })}
        </div>
      </article>
    </Main>
    <Footer />
    <style jsx>{`
      article {
        width: 100%;
      }
      .content {
        margin-top: 6rem;
      }
      @media (max-width: 600px) {
        article {
          width: 100%;
          padding: 2rem;
        }
      }
    `}</style>
  </Layout>
)
}

export const getStaticPaths = async () => {
  const data : any = await request({
    query: ALL_ARTICLES,
    variables: {},
    preview: false
  });
  
  return {
    paths: data.allArticles.map(article => ({ params: { slug: article.slug }})),
    fallback: false // See the "fallback" section below
  };
}

export const getStaticProps = async ({ params }) => {

  const data = await request({
    query: SINGLE_ARTICLE,
    variables: { slug: params.slug },
    preview: false
  });

  return {
    props: {
      data
    }
  }
}

const ALL_ARTICLES = `
query {
  allArticles {
    slug
  }
}
`

const SINGLE_ARTICLE = `query Article($slug: String) {
  article(filter: {
    slug: { eq: $slug }
  }) {
    _createdAt
    _updatedAt
    title
    author {
      firstName
      lastName
    }
    block {
      ...on QuotationRecord {
        __typename
        author
        quote
      }
      ...on ContentRecord {
        __typename
        headline
        content
        citations {
          description
          source
        }
      }
      ...on InfoCardRecord {
        cardType
      }
      ...on ImageRecord {
        __typename
        content
        caption
        size
        media {
          responsiveImage {
            sizes
            src
            srcSet
          }
          blurUpThumb
        }
      }

    }
  }
}
`

export default Article
