import particles from 'particles.js'
import Head from 'next/head'
import getMetaData from 'metadata-scraper'
import { Card, CardGrid, ICard, Layout, Main, Title, Footer } from '../components'
import { request } from "../lib/datocms";

interface IDesignSystemsProps {
    designSystems: IDesignSystem[]
    data?: any;
}

interface IDesignSystem {
    title: string;
    href: string;
    imgSrc?: string;
}

// const DynamicComponent = dynamic(() => import('particles.js/particles.js'))

const DesignSystems: React.FC<IDesignSystemsProps> = ({ designSystems, data }) => {
  
  return (
    <Layout>
      <div>
        <Title href="/" content="Ventures & Adventures" size='xs' />
      </div>
      <Main>
        <Title content="Design Systems" />
        <p className="description">
          A collection of my favorite react component libraries.
        </p>
        <CardGrid cards={designSystems} />
      </Main>
      <Footer />
      <style jsx>{`

        a {
          color: inherit;
          text-decoration: none;
        }

        a:not([href*='ventures-adventures.com']):not([href^='#']):not([href^='/']) .card h3:after {
          font-family: 'FontAwesome';
          content: " \f08e";
        }

      `}</style>

    </Layout>
  )
}

const INDEX_QUERY = `
query {
    allDesignSystems {    
        name
        externalUrl
        slug
    }
    
    _allDesignSystemsMeta {
        count
    }
}
`

export const getStaticProps = async () => {
  // Get external data from the file system, API, DB, etc.
  const data : any = await request({
    query: INDEX_QUERY,
    variables: {},
    preview: false
  });
  // The value of the `props` key will be

  const designSystems = await Promise.all(data.allDesignSystems.map(async system => {
    const metadata = await getMetaData(system.externalUrl)

    return { title: system.name, href: system.externalUrl, imgSrc: metadata.image ? metadata.image : null, content: metadata.description ? metadata.description : null }
  }))
  
  //  passed to the `DesignSystems` component  
  return {
    props: {
      data,
      designSystems
    }
  }
}

export default DesignSystems
