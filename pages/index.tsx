import particles from 'particles.js'
import Head from 'next/head'
import { Card, CardGrid, ICard, Layout, Main, Title, Footer } from '../components'
import { request } from "../lib/datocms";

interface IHomeProps {
  projects: ICard[]
  articles: IArticle[]
  data?: any;
}

interface IArticle {
  title: string;
  slug: string;
  status?: string;
  _status: string;
}

// const DynamicComponent = dynamic(() => import('particles.js/particles.js'))

const Home: React.FC<IHomeProps> = ({ projects, articles, data }) => (
  <Layout>
    <Main>
      <Title content="Ventures & Adventures" />

      <p className="description">
        A sweat equity venture portfolio.
      </p>

      <Title content="Projects:" size='s' />
      <CardGrid cards={projects} />

      <Title content="Articles:" size='s' />
      {articles.map(article => 
        <Title h={3} content={`${article.status === 'draft' ? '[DRAFT] ' : ''}${article.title}`} size='xs' href={`/articles/${article.slug}`} key={article.slug} />
      )}

    </Main>
    <Footer />
    <style jsx>{`

      a {
        color: inherit;
        text-decoration: none;
      }

      a:not([href*='ventures-adventures.com']):not([href^='#']):not([href^='/']) .card h3:after {
        font-family: 'FontAwesome';
        content: " \f08e";
      }

    `}</style>

  </Layout>
)

const INDEX_QUERY = `
query {
  allArticles {
    title
    slug
    publishStatus
  }
  allProjects(filter: { publishStatus: { eq:"published" }}) {
    title
    description
    externalUrl
  }
}
`

export const getStaticProps = async () => {
  // Get external data from the file system, API, DB, etc.
  const data : any = await request({
    query: INDEX_QUERY,
    variables: {},
    preview: false
  });
  // The value of the `props` key will be
  //  passed to the `Home` component
  return {
    props: {
      data,
      projects: data.allProjects.map(project => ({ title: project.title, content: project.description, href: project.externalUrl })),
      articles: data.allArticles.map(article => ({ title: article.title, slug: article.slug, status: article.publishStatus ? article.publishStatus : null }))
    }
  }
}

export default Home
